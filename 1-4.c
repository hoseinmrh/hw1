#include <stdio.h>

int capital(int c){
    if (c>=97 && c<=122){
        c -= 32;
    }
    return c;
}

int main(){
    int c;
    int flag = 0;
    int ascending = 1;
    while((c = getchar()) != '\n'){
        if(c == '*'){
            if(flag == 0){
                flag++;
                ascending = 1;
            } else if (flag == 2){
                flag--;
                ascending = 0;
            } else if (flag == 1){
                if (ascending == 1)flag++;
                else flag--;
            }
            continue;
        }
        if (flag == 2){
            c = capital(c);
        }
        putchar(c);
    }
    return 0;
}
