#include <stdio.h>

int capital(int c){
    if(c>=97 && c<=122){
        c -= 32;
    }
    return c;
}

int main(){
    int cap=0;
    int c;
    while((c=getchar())!= '\n'){
        if(cap==0){
            cap=1;
            c=capital(c);
        } else{
            if(c == ' '){
                cap=0;
            }
        }
        putchar(c);
    }
    return 0;
}
