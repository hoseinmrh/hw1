#include <stdio.h>

int get_num(){
char c = getchar();
int a = 0;
int neg = 0;

if(c == '-') {
   neg = 1;
   c = getchar();
}

while(isdigit(c)) {
    a = a*10 + (c - '0');
    c = getchar();
}

if(neg == 1) {
    a = a * -1;
}
return a;
}

int main(){
    int number , x , i=0 , s=0 , max_n=0;
    int avg;
    number=get_num();
    for ( i; i<number ; i++ ) {
        x = get_num();
        s = s + x;
        if(max_n < x){
            max_n = x;
        }
}
   avg = s/number;
   printf("%d %d" , max_n , avg);

}
