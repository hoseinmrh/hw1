#include <stdio.h>

int get_num(){
char c = getchar();
int a = 0;
int neg = 0;

if(c == '-') {
   neg = 1;
   c = getchar();
}

while(isdigit(c)) {
    a = a*10 + (c - '0');
    c = getchar();
}

if(neg == 1) {
    a = a * -1;
}
return a;
}
int main(){
int x,y,s;
x= get_num();
y= get_num();
s = x+y;
printf("%d" , s);
return 0;
}
